//
//  QuestionView.swift
//  test
//
//  Created by admin on 7/19/16.
//  Copyright © 2016 admin. All rights reserved.
//

import UIKit

class QuestionView: UIView {
    
    var color = UIColor.lightGrayColor() {
        didSet {
            setNeedsDisplay()
        }
    }
    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
        let triangeHeight = rect.size.height / 2.0
        let path = UIBezierPath()
        path.moveToPoint(rect.origin)
        path.addLineToPoint(CGPoint(x: rect.origin.x + rect.size.width - triangeHeight, y: rect.origin.y))
        path.addLineToPoint(CGPoint(x: rect.origin.x + rect.size.width - 10.0, y: rect.origin.y + rect.size.height / 2.0))
        
        
        path.addLineToPoint(CGPoint(x: rect.origin.x + rect.size.width - triangeHeight, y: rect.origin.y + rect.size.height))
        path.addLineToPoint(CGPoint(x: rect.origin.x, y: rect.origin.y + rect.size.height))
        path.addLineToPoint(CGPoint(x: rect.origin.x, y: rect.origin.y))
        path.addArcWithCenter(CGPoint(x: rect.origin.x + rect.size.width - 10.0, y: rect.origin.y + rect.size.height / 2.0), radius: 10.0, startAngle: 0.0, endAngle: 2 * CGFloat(M_PI), clockwise: true)
        path.closePath()
        
        // fill
        color.setFill()
        // stroke
        path.lineWidth = 1.0
        let strokeColor = UIColor.clearColor()
        strokeColor.setStroke()
        
        path.fill()
        path.stroke()
    }
    
}

//
//  Cell.swift
//  test
//
//  Created by admin on 7/20/16.
//  Copyright © 2016 admin. All rights reserved.
//

import UIKit

enum ConnectionType {
    case empty
    case one
    case plenty
}

enum Side {
    case left
    case right
}

class Node {
    var connectedNodes = [Node]()
    var color: UIColor = UIColor.appGrayColor()
    var connectionType = ConnectionType.empty
    var indexPath: NSIndexPath?
    var side: Side
    var title: String
    var selected = false
    var highlitedColor: UIColor {
        get {
            return color.darkerColor()!
        }
    }
    init(title: String, side: Side) {
        self.title = title
        self.side = side
    }
    
    static func initListsFromDict(dict: [String: AnyObject]) -> ([Node], [Node])? {
        if let test = dict["test"] as? [String: AnyObject], let left = test["left"] as? [String], let right = test["right"] as? [String] {
            print(left)
            print(right)
            var leftNodes = [Node]()
            var rightNodes = [Node]()
            for title in left {
                leftNodes.append(Node(title: title, side: .left))
            }
            for title in right {
                rightNodes.append(Node(title: title, side: .right))
            }
            return (leftNodes, rightNodes)
        }
        return nil
    }
}



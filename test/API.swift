//
//  API.swift
//  test
//
//  Created by admin on 7/20/16.
//  Copyright © 2016 admin. All rights reserved.
//

import Foundation
import Alamofire
import SVProgressHUD

class API {
    
    static func getLists(callbackOnSuccess: (([Node], [Node])) -> Void, callbackOnFailure: () -> Void) {
        let urlString = "http://balasdevserver.trinitydigital.ru/data"
        SVProgressHUD.show()
        Alamofire.request(.GET, urlString).responseJSON { (response) in
            SVProgressHUD.dismiss()
            if let dict = response.result.value as? [String: AnyObject] {
                if let tuple = Node.initListsFromDict(dict) {
                    callbackOnSuccess(tuple)
                } else {
                    callbackOnFailure()
                }
            } else {
                callbackOnFailure()
            }
        }
    }
    
    
    
}
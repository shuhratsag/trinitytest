//
//  UIColorExtension.swift
//  test
//
//  Created by admin on 7/20/16.
//  Copyright © 2016 admin. All rights reserved.
//

import UIKit

extension UIColor {
    func darkerColor() -> UIColor? {
        var red : CGFloat = 0.0, green: CGFloat = 0.0, blue: CGFloat = 0.0, alpha: CGFloat = 0.0
        if self.getRed(&red, green: &green, blue: &blue, alpha: &alpha) {
            return UIColor(red: max(red - 0.2, 0.0), green: max(green - 0.2, 0.0), blue: max(blue - 0.2, 0.0), alpha: 1.0)
        } else {
            return nil
        }
    }
    
    func lighterColor() -> UIColor? {
        var red : CGFloat = 0.0, green: CGFloat = 0.0, blue: CGFloat = 0.0, alpha: CGFloat = 0.0
        if self.getRed(&red, green: &green, blue: &blue, alpha: &alpha) {
            return UIColor(red: min(red + 0.2, 1.0), green: min(green + 0.2, 1.0), blue: min(blue + 0.2, 1.0), alpha: 1.0)
        } else {
            return nil
        }
    }
    
    static func appGrayColor() -> UIColor {
        return UIColor(white: 0.8, alpha: 1.0)
    }
}

//
//  ViewController.swift
//  test
//
//  Created by admin on 7/19/16.
//  Copyright © 2016 admin. All rights reserved.
//

import UIKit
import RandomColorSwift



class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var leftTableView: UITableView!
    @IBOutlet weak var rightTableView: UITableView!
    
    var connectionsAreStraight = true
    var cellHeight: CGFloat!
    
    // true if looking for pair within right cells
    var matchingRight = false
    // true if looking for pair within left cells...
    var matchingLeft = false
    // ... for selected (answer or question) = node
    var selectedNode: Node?
    
    var leftNodes = [Node]()
    var rightNodes = [Node]()
    
    // we can draw connection between answer and question with info gathered in Connection instance
    var connections = [Connection]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cellHeight = self.view.frame.height / 4.0
        
        leftTableView.delegate = self
        leftTableView.dataSource = self
        rightTableView.delegate = self
        rightTableView.dataSource = self
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ViewController.deviceOrientationDidChande), name: UIDeviceOrientationDidChangeNotification, object: nil)
        
        // get arrays of questions and answers
        getLists()
    }
    
    func getLists() {
        API.getLists({
            // onSuccess
            (leftNodes, rightNodes) in
            self.leftNodes = leftNodes
            self.rightNodes = rightNodes
            self.leftTableView.reloadData()
            self.rightTableView.reloadData()
        }) {
            // onFailure
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table View Data Source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == leftTableView {
            return leftNodes.count
        } else {
            return rightNodes.count
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if tableView == leftTableView {
            let cell = tableView.dequeueReusableCellWithIdentifier("QuestionCell") as! TableViewCell
            let node = leftNodes[indexPath.row]
            node.indexPath = indexPath
            cell.titleLabel.text = node.title
            cell.questionView.color = node.selected ? node.highlitedColor : node.color
            return cell
        } else {
            let cell = tableView.dequeueReusableCellWithIdentifier("AnswerCell") as! TableViewCell
            let node = rightNodes[indexPath.row]
            node.indexPath = indexPath
            cell.titleLabel.text = node.title
            cell.answerView.color = node.selected ? node.highlitedColor : node.color
            return cell
        }
    }
    
    // MARK: - Table View Delegate
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if tableView == leftTableView {
            if matchingLeft {
                nodeMatched(atIndexPath: indexPath)
                matchingLeft = false
                selectedNode?.selected = false
                selectedNode = nil
            } else {
                if selectedNode?.indexPath == indexPath {
                    selectedNode?.selected = false
                    selectedNode = nil
                    matchingRight = false
                } else {
                    selectedNode?.selected = false
                    let node = leftNodes[indexPath.row]
                    selectedNode = node
                    matchingRight = true
                    selectedNode?.selected = true
                }
            }
        } else {
            if matchingRight {
                nodeMatched(atIndexPath: indexPath)
                matchingRight = false
                selectedNode?.selected = false
                selectedNode = nil
            } else {
                if selectedNode?.indexPath == indexPath {
                    selectedNode?.selected = false
                    selectedNode = nil
                    matchingLeft = false
                } else {
                    selectedNode?.selected = false
                    let node = rightNodes[indexPath.row]
                    selectedNode = node
                    matchingLeft = true
                    selectedNode?.selected = true
                }
            }
        }
        leftTableView.reloadData()
        rightTableView.reloadData()
        
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        redrawCurves()
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return cellHeight
    }
    
    // MARK: - Connections Logic
    
    func nodeMatched(atIndexPath indexPath: NSIndexPath) {
        if let color = connectNodesIfPossible(atIndexPath: indexPath) {
            let leftIndexPath: NSIndexPath
            let rightIndexPath: NSIndexPath
            switch selectedNode!.side {
            case .left:
                leftIndexPath = selectedNode!.indexPath!
                rightIndexPath = indexPath
            case .right:
                leftIndexPath = indexPath
                rightIndexPath = selectedNode!.indexPath!
            }
            let leftPoint = connectionPointForLeftCell(atIndexPath: leftIndexPath)
            let rightPoint = connectionPointForRightCell(atIndexPath: rightIndexPath)
            let shapeLayer = shapeLayerWithCurve(fromPoint: leftPoint, toPoint: rightPoint, withColor: color)
            let connection = Connection(leftIndexPath: leftIndexPath, rightIndexPath: rightIndexPath, shapeLayer: shapeLayer, color: color)
            self.connections.append(connection)
            self.bgView.layer.addSublayer(shapeLayer)
        }
    }

    
    // Returns color for connected nodes or NIL if connection is impossible
    func connectNodesIfPossible(atIndexPath indexPath: NSIndexPath) -> UIColor? {
        let matchedNode: Node
        switch selectedNode!.side {
        case .left:
            matchedNode = rightNodes[indexPath.row]
        case .right:
            matchedNode = leftNodes[indexPath.row]
        }
        if matchedNode.connectionType == .one || selectedNode!.connectionType == .one {
            return nil
        } else if matchedNode.connectionType == .plenty && selectedNode!.connectionType == .plenty {
            return nil
        }
        // will return true
        let color: UIColor
        if matchedNode.connectionType == .empty && selectedNode!.connectionType == .empty {
            matchedNode.connectionType = .plenty
            selectedNode!.connectionType = .plenty
            selectedNode!.connectedNodes.append(matchedNode)
            matchedNode.connectedNodes.append(selectedNode!)
            
            color = randomColor(hue: .Random, luminosity: .Light)
            matchedNode.color = color
            selectedNode!.color = color
            paintCellsAtNodes([matchedNode, selectedNode!], color: color)
        } else {
            if matchedNode.connectionType == .plenty {
                color = matchedNode.color
                matchedNode.connectedNodes.append(selectedNode!)
                makeAllConnectedNodesSingleAndPaint(matchedNode)
            } else {
                color = selectedNode!.color
                selectedNode!.connectedNodes.append(matchedNode)
                makeAllConnectedNodesSingleAndPaint(selectedNode!)
            }
        }
        return color
    }
    
    func makeAllConnectedNodesSingleAndPaint(node: Node) {
        for connectedNode in node.connectedNodes {
            connectedNode.connectionType = .one
            connectedNode.color = node.color
        }
        var nodesToPaint = [node]
        nodesToPaint.appendContentsOf(node.connectedNodes)
        paintCellsAtNodes(nodesToPaint, color: node.color)
    }
    
    
    // MARK: - Calculator

    func connectionPointForLeftCell(atIndexPath indexPath: NSIndexPath) -> CGPoint {
        let rect = absoluteRectForLeftCell(atIndexPath: indexPath)
        return CGPoint(x: rect.origin.x + rect.width - 10.0, y: rect.origin.y + rect.height / 2.0)
    }
    
    func connectionPointForRightCell(atIndexPath indexPath: NSIndexPath) -> CGPoint {
        let rect = absoluteRectForRightCell(atIndexPath: indexPath)
        return CGPoint(x: rect.origin.x + 10.0, y: rect.origin.y + rect.height / 2.0)
    }
    
    func absoluteRectForLeftCell(atIndexPath indexPath: NSIndexPath) -> CGRect {
        let x = leftTableView.frame.origin.x
        let y = leftTableView.frame.origin.y + CGFloat(indexPath.row) * cellHeight - leftTableView.contentOffset.y
        let width = leftTableView.rectForRowAtIndexPath(indexPath).size.width
        return CGRect(x: x, y: y, width: width, height: cellHeight)
    }
    
    func absoluteRectForRightCell(atIndexPath indexPath: NSIndexPath) -> CGRect {
        let x = rightTableView.frame.origin.x
        let y = rightTableView.frame.origin.y + CGFloat(indexPath.row) * cellHeight - rightTableView.contentOffset.y
        let width = rightTableView.rectForRowAtIndexPath(indexPath).size.width
        return CGRect(x: x, y: y, width: width, height: cellHeight)
    }
    
    func controlPointsForBezier(withStartPoint startPoint: CGPoint, endPoint: CGPoint) -> (CGPoint, CGPoint) {
        var cp1 = CGPoint()
        var cp2 = CGPoint()
        cp1.x = (endPoint.x + startPoint.x) / 2.0
        cp2.x = cp1.x
        cp1.y = startPoint.y
        cp2.y = endPoint.y
        return (cp1, cp2)
    }
    
    // MARK: - Drawing
    
    func shapeLayerWithCurve(fromPoint startPoint: CGPoint, toPoint endPoint: CGPoint, withColor color: UIColor) -> CAShapeLayer {
        let path = UIBezierPath()
        path.moveToPoint(startPoint)
        let (cp1, cp2) = controlPointsForBezier(withStartPoint: startPoint, endPoint: endPoint)
        path.addCurveToPoint(endPoint, controlPoint1: cp1, controlPoint2: cp2)
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = path.CGPath
        shapeLayer.lineWidth = 5.0
        shapeLayer.strokeColor = color.CGColor
        shapeLayer.fillColor = UIColor.clearColor().CGColor
        return shapeLayer
    }
    
    func redrawCurves() {
        for connection in connections {
            connection.shapeLayer.removeFromSuperlayer()
            let leftPoint = self.connectionPointForLeftCell(atIndexPath: connection.leftIndexPath)
            let rightPoint = self.connectionPointForRightCell(atIndexPath: connection.rightIndexPath)
            let shapeLayer = shapeLayerWithCurve(fromPoint: leftPoint, toPoint: rightPoint, withColor: connection.color)
            connection.shapeLayer = shapeLayer
            self.bgView.layer.addSublayer(shapeLayer)
        }
    }
    
    func removeCurves() {
        for connection in connections {
            connection.shapeLayer.removeFromSuperlayer()
        }
    }
    
    func paintCellsAtNodes(nodes: [Node], color: UIColor) {
        for node in nodes {
            if node.side == .left {
                if let cell = leftTableView.cellForRowAtIndexPath(node.indexPath!) as? TableViewCell {
                    cell.questionView.color = node.color
                }
            } else {
                if let cell = rightTableView.cellForRowAtIndexPath(node.indexPath!) as? TableViewCell {
                    cell.answerView.color = node.color
                }
            }
        }
    }
    
    // MARK: - Nofifications Listener
    
    func deviceOrientationDidChande() {
        cellHeight = self.view.frame.height / 4.0
        self.leftTableView.reloadData()
        self.rightTableView.reloadData()
        self.redrawCurves()
    }
    
    // MARK: - Actions
    
    @IBAction func clean(sender: UIButton) {
        self.removeCurves()
        self.connections.removeAll()
        for node in self.leftNodes {
            node.color = UIColor.appGrayColor()
            node.connectedNodes = [Node]()
            node.connectionType = .empty
            node.selected = false
            //node.indexPath = nil
        }
        for node in self.rightNodes {
            node.color = UIColor.appGrayColor()
            node.connectedNodes = [Node]()
            node.connectionType = .empty
            node.selected = false
            //node.indexPath = nil
        }
        self.leftTableView.reloadData()
        self.rightTableView.reloadData()
    }
    
    
    // MARK: - Alert
    
    func presentDataUnavailableAlert() {
        let alertController = UIAlertController(title: nil, message: "Не удалось загрузить данные", preferredStyle: .Alert)
        let tryAgainAction = UIAlertAction(title: "Повторить снова", style: .Default) { (action) in
            self.getLists()
        }
        alertController.addAction(tryAgainAction)
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
}


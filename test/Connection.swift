//
//  Connection.swift
//  test
//
//  Created by admin on 7/19/16.
//  Copyright © 2016 admin. All rights reserved.
//

import UIKit

class Connection {
    var leftIndexPath: NSIndexPath
    var rightIndexPath: NSIndexPath
    var shapeLayer: CAShapeLayer
    var color: UIColor
    init(leftIndexPath: NSIndexPath, rightIndexPath: NSIndexPath, shapeLayer: CAShapeLayer, color: UIColor) {
        self.leftIndexPath = leftIndexPath
        self.rightIndexPath = rightIndexPath
        self.shapeLayer = shapeLayer
        self.color = color
    }
}
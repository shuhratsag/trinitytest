//
//  AnswerView.swift
//  test
//
//  Created by admin on 7/19/16.
//  Copyright © 2016 admin. All rights reserved.
//

import UIKit

class AnswerView: UIView {
    
    var color = UIColor.lightGrayColor() {
        didSet {
            setNeedsDisplay()
        }
    }
    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
        let triangeHeight = rect.size.height / 2.0
        let path = UIBezierPath()
        path.moveToPoint(CGPoint(x: rect.size.width, y: 0.0))
        path.addLineToPoint(CGPoint(x: triangeHeight, y: 0.0))
        path.addLineToPoint(CGPoint(x: 10.0, y: rect.size.height / 2.0))
        
        path.addLineToPoint(CGPoint(x: triangeHeight, y: rect.size.height))
        path.addLineToPoint(CGPoint(x: rect.size.width, y: rect.size.height))
        path.addLineToPoint(CGPoint(x: rect.size.width, y: 0.0))
        path.addArcWithCenter(CGPoint(x: 10.0, y: rect.size.height / 2.0), radius: 10.0, startAngle: 2 * CGFloat(M_PI), endAngle: 0.0, clockwise: false)
        path.closePath()
        
        // fill
        color.setFill()
        // stroke
        path.lineWidth = 1.0
        let strokeColor = UIColor.clearColor()
        strokeColor.setStroke()
        
        path.fill()
        path.stroke()
    }
 

}

# README #

This is a sample test project called "Questions and Answers".
This iOS app demonstrates usage of UITableView, UIShapeLayer.
Custom logic is implemented to animate interaction between "Question" and "Answer" views.